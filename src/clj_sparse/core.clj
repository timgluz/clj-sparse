;;sparsematrix module
;;clojure wrappers around colt0.74, which also powers clojure/Incanter computations
;;
;;Look javadocs: 
;;http://acs.lbl.gov/software/colt/api/index.html

(ns clj-sparse.core
	(:refer-clojure :exclude [vector?])
  	(:import [cern.colt.matrix.tdouble DoubleFactory1D DoubleFactory2D]
           [cern.colt.matrix.tdouble.impl SparseDoubleMatrix1D 
                                          SparseDoubleMatrix2D]
           [cern.colt.matrix.tdouble.algo DenseDoubleAlgebra SmpDoubleBlas]
           [cern.jet.math.tdouble DoubleFunctions]
           ))

;;Helper functions
(defn- max-pos [val-map]
  "returns max position form val-map, useful for to specify max-size of sparse vector"
  (apply max (keys val-map)))

;;GetSetters and public wrapper around Java interface
(defn setq 
  "Does quickSetting to Matrix, aka doesnt control limits of matrix"
  ([^SparseDoubleMatrix1D spmatrix ^Integer col val]
    (.setQuick spmatrix col (double val))) 
  ;;set value for  2D
  ([^SparseDoubleMatrix2D spmatrix ^Integer row ^Integer col  ^Double val]
    (.setQuick spmatrix row col val)) 
)

(defn getq
  "Does quickGetting from given matrix."
  ([^SparseDoubleMatrix1D spmatrix ^Integer col]
    (.getQuick spmatrix col))
  ;;get value for sparse 2d matrix
  ([^SparseDoubleMatrix2D spmatrix ^Integer row ^Integer col]
    (.getQuick spmatrix row col))
  )

(defn- get-class [obj]
  "returns name of Java class"
  (str (.getClass obj)))

(defn- match-class [pattern obj]
  (let [class-name (get-class obj)]
    (not 
      (nil? (re-find pattern class-name)))
    ))

(defn sparse? [mat]
	(match-class #"Sparse" mat))

(defn vector? [vec]
  (match-class #"Matrix1D" vec))

(defn matrix? [mat]
  (match-class #"Matrix2D" mat))

(defn sparse-vector 
  "Creates new sparse vector  with given value map, 
  which has given structure {pos val, pos2 val2}
  "
  ([] (SparseDoubleMatrix1D. 1))
  ([^Integer vector-size] 
    (SparseDoubleMatrix1D. vector-size))
  ([^Integer vector-size val-map]
   "Accepts hash-map with given structure {pos1 val1, pos2 val2}"
    (let [
          spmatrix (SparseDoubleMatrix1D. vector-size)]
          ;initialize row-vector
          (doseq [[pos val] val-map]
                (setq spmatrix pos val))
           spmatrix
      )))

(defn init [mat val]
  "Sets all cells to val" 
  (.assign mat (double val)))

(defn equal? [mat1 mat2]
  (.equals mat1 mat2))

(defn col 
  "Returns given column"
  ([^SparseDoubleMatrix2D mat, ^Integer col-nr] (.viewColumn mat col-nr)))

(defn row 
  "Returns given row"
  ([^SparseDoubleMatrix2D mat ^Integer row-nr] (.viewRow mat row-nr)))

(defn transpose [mat]
  "returns a new dice (transpositions view) by swapping axeses."
  (.viewDice mat))

(defn t [mat]
  "short-handed alias for transpose"
  (transpose mat))

(defn size [mat]
  (if (instance? SparseDoubleMatrix1D mat)
    (vector 1 (.size mat))
    (vector (.rows mat) (.columns mat))
    ))

(defn copy [mat]
  "returns copy of given matrix"
  (.copy mat))

(defn rows [mat]
  "returns count of rows given matrix has"
  (first (size mat)))

(defn cols [mat]
  "returns count of cols given matrix has"
  (second (size mat)))

(defn sparse-matrix 
  "Creates new sparse matrix"
  ([^Integer rows ^Integer cols] (SparseDoubleMatrix2D. rows cols))
  ([^Integer rows ^Integer cols val-map]
   "Creates and initialize new sparse matrix. Val-map have to have give structure {[row, col] val}"
    (let [sm (sparse-matrix rows cols)]
      (doseq [[pos val] val-map]
        (setq sm (first pos) (second pos) val))
      sm
    )))


(defn eyes [size]
  "Builds sparse matrix, on which diagonals are ones"
  (let [mat (sparse-matrix size size)]
    (dorun
      (map (fn [row] (setq mat row row 1.0)) 
           (range 0 size)))
    mat
    ))

