(ns clj-sparse.ops
	(:refer-clojure :exclude [vector?])
	(:use [clj-sparse.core])
	(:import [cern.colt.matrix.tdouble DoubleFactory1D DoubleFactory2D]
           [cern.colt.matrix.tdouble.impl SparseDoubleMatrix1D 
                                          SparseDoubleMatrix2D]
           [cern.colt.matrix.tdouble.algo DenseDoubleAlgebra SmpDoubleBlas]
           [cern.jet.math.tdouble DoubleFunctions]
           )) 

;;Construct math objects

(def BLAS (SmpDoubleBlas.))
(def DDAlgebra (DenseDoubleAlgebra.))

;; OPERATIONS on matrix 

(defn dot [m1 m2]
  (.ddot BLAS m1 m2))

(defn inverse [mat]
  (.inverse DDAlgebra mat))

(defn sum [m1]
  "calculates sum of sparse-vector items"
  (.zSum m1))

(defn sqrt [m1]
  "applies SQRT for every matrix value
  Read more: 
  http://incanter.org/docs/parallelcolt/api/cern/jet/math/tdouble/DoubleFunctions.html
  "
  (.assign m1 DoubleFunctions/sqrt))


(defn mat-apply 
  "Applies Colt's DoubleFunction to every item of given matrix"
  ([mat dfunc] (.assign mat dfunc))
  ([mat dfunc arg1] (mat-apply mat (DoubleFunctions/bindArg1 dfunc arg1)))
  )

(defn mult-mat-mat [m1 transpose1 m2 transpose2 ]
  "Lower level function that multiplies 2 matrixes together"
  (let [result-size [(if (= transpose1 false) (rows m1) (cols m1)) 
                     (if (= transpose2 false) (cols m2) (rows m2))]
        result-mat (sparse-matrix (first result-size) (second result-size))]
    (do
      (.dgemm BLAS transpose1 transpose2 1.0 m1 m2 1.0 result-mat)
      result-mat) 
  ))

(defn mult-mat-vec [^SparseDoubleMatrix2D m1 transpose1 ^SparseDoubleMatrix1D  m2]
  "Lower level function that multiplies together matrix and vector"
  (let [result-size (if (= transpose1 false) (rows m1) (cols m1))
        result-vec (sparse-vector result-size)]
    (do 
      (.dgemv BLAS transpose1 1.0 m1 m2 1.0 result-vec)
      result-vec)
    ))

(defn plus [m1 m2]
  "Performs matrix "
  (cond
    (and (matrix? m1) (number? m2)) (mat-apply m1 DoubleFunctions/plus (double m2))
    (and (number? m1) (matrix? m2)) (mat-apply m2 DoubleFunctions/plus (double m1)) 
    (and (vector? m1) (vector? m2)) (.assign m1 m2 DoubleFunctions/plus)
    (and (matrix? m1) (matrix? m2)) (.assign m1 m2 DoubleFunctions/plus)
    ))

(defn minus [m1 m2]
  "Performs matrix  with scalar or matrix"
  (cond 
    (and (matrix? m1) (number? m2)) (.assign m1 (double m2) DoubleFunctions/minus)
    (and (number? m1) (matrix? m2)) (do 
                                        (.assign m2 DoubleFunctions/neg)
                                        (.assign m2 (DoubleFunctions/plus (double m1)))) 
    (and (vector? m1) (vector? m2)) (.assign m1 m2 DoubleFunctions/minus)
    (and (matrix? m1) (matrix? m2)) (if-not (equal? m1 m2)
                                      (.assign m1 m2 DoubleFunctions/minus)
                                      0 ;if subtracting to equal matrices
                                      )

    ))

(defn mult [m1 m2]
  "Performs matrix multiplication with scalar or matrix"
  (cond 
    (and (number? m1) (number? m2)) (.apply DoubleFunctions/mult m1 m2) 
    (and (matrix? m2) (number? m1)) (mat-apply m2 DoubleFunctions/mult m1) 
    (and (not (number? m1))
         (number? m2)) (mat-apply m1 DoubleFunctions/mult m2)
    (and (vector? m1) (vector? m2)) (dot m1 m2)
    (and (matrix? m1) (vector? m2)) (mult-mat-vec m1 false m2)
    (and (matrix? m1) (matrix? m2)) (mult-mat-mat m1 false m2 false)    
    ))

(defn div [m1 m2]
  ;;TODO:  finish
  ;; number/matrix
  ;;matrix/number
  ;;vector/vector
  ;;matrix/matrix
  )

