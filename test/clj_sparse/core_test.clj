(ns clj-sparse.core-test
	(:refer-clojure :exclude [vector?])
  	(:use clojure.test  		
        clj-sparse.core
        clj-sparse.ops))

(def test-val-map {0 1.0, 1 4.0, 2 2.0, 3 0.5 4 -1.0})

(deftest test-vector
	(testing "creating new sparse vectors"
		(is (= [1 1] (size (sparse-vector))) "Emtpy vector")
		(is (= [1 10] (size (sparse-vector 10))) "Empty vector with specified size")
		(is (= [1 5] (size (sparse-vector 
					(count test-val-map) test-val-map))) "New vector with initialized values")
	)
	(testing "methods of sparse-vector"
		(def vec1 (sparse-vector (count test-val-map) test-val-map))
		(is (= true (vector? vec1)) "Is defined vector still vector")
		(is (= 4.0 (getq vec1 1)) "Getting value from given position")
		(is (= -1.0 (getq vec1 4)) "Getting last value of test vector")		
	)
	(testing "operations on vectors")

	)