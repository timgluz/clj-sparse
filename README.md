# clj-sparse

A Clojure library designed to perform calculations with sparse matrices.

Still working on this, but hopefully it'll be integrated to Incanter library someday.


## Usage

 * Install Incanter, because it includes required opencolt library 

 * clone this library

 * run tests

    >>> lein test 

 *  run repl

;; Simple example
(:use [clj-sparse core ops])
(def vec1 (sparse-vector 3 {0 1.0, 1 -1.0}))
(def vec2 (sparse-vector 3 {2 2.0}))
(plus vec1 vec2)
(dot vec1 vec2)

## License

Copyright © 2012 TimGluz

Distributed under the Eclipse Public License, the same as Clojure.
